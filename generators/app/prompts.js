'use strict';
const utils = require('../../utils/all');

const defaults = {
  companyName:'Adistec'
}



module.exports = [
  {
    type: 'input',
    name: 'companyName',
    message: 'Please choose your company name',
    default: defaults.companyName
  },
  {
    type: 'input',
    name: 'appName',
    message: 'Please choose your application name',
    default: utils.yeoman.getAppName()
  }
];
