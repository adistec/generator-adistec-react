import React from 'react';
import { connect } from 'react-redux';
import { Header , Loader , Footer } from 'adistec-react-components';
import { getUserInfo } from '../actions/userActions';
import {FormattedMessage} from 'react-intl';
import MenuItem from 'material-ui/MenuItem';
import { logo } from '../assets/img/adistec-logo-white.png';

const drawerItems =
    [{label: <FormattedMessage id='MainLayout.drawer.firstItem'/>, path: ''},
    {label: <FormattedMessage id='MainLayout.drawer.secondItem'/>, path: 'request',permissions:['ADISTEC_REACT_COMPONENTS_USER']}];

class MainLayout extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.getUserInfo();
    }

    getMenuItems = () => {
           return  [
               <MenuItem leftIcon={<i style={{color: '#005fab'}} className="material-icons">power_settings_new</i>} primaryText="Log out"   onTouchTap={() => {
               window.location.href = _API + '/logout';
               }}/>
           ]
        };

    render() {
        return (
            this.props.user.auth ?
                <div className="appWrapper">
                    <Header drawerItems={drawerItems}
                            menuTitle={<FormattedMessage id='MainLayout.appTitle'/>}
                            menuItems={this.getMenuItems()}
                            username={`${this.props.user.name} ${this.props.user.lastName}`}
                            userEmail={this.props.user.email}
                            logoSrc={logo}
                            companyName={this.props.user.companyName ? this.props.user.companyName : undefined}
                            permissions={this.props.user.permissions}/>
                    <div id="main-wrapper">
                    {this.props.counter.ready != 0 ? <Loader /> : null}
                        <main style={this.props.counter.ready != 0 ? {opacity:0.2} : null}>
                            {this.props.children}
                        </main>
                    </div>
                    <Footer/>
                </div> : null
        );
    }
}

export default connect(
    state => {
        return {
            user: state.user,
            counter: state.loader.counter
        }
    }, { getUserInfo }
)(MainLayout);
