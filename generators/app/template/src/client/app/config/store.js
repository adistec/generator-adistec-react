import reduxThunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import { cacheEnhancer } from 'redux-cache';
import reducers from '../reducers/index';
import DevTools from './devtools';

const createStoreWithMiddleware = compose(
    applyMiddleware(reduxThunk),cacheEnhancer()
   )(createStore);

export const store = _ENV === 'dev' || 'sit' ? createStoreWithMiddleware(reducers,DevTools.instrument()) : createStoreWithMiddleware(reducers);
