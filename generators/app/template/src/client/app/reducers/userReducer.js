import { USER_INFO } from '../actions/types';

const INITIAL_STATE = {name: '', lastName: '', email: '', open: false, locale: 'en', auth: false, roles:[],permissions:['ADISTEC_REACT_COMPONENTS_USER']};

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case USER_INFO:
            return {...state, name: action.payload.name, lastName: action.payload.lastname,
            	email: action.payload.email, auth: true, roles: action.payload.roles,permissions: action.payload.permissions, locale: action.payload.locale};
    }

    return state;
}
