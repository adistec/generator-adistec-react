import { combineReducers } from 'redux';
import userReducer from './userReducer';
import loaderReducer from './loaderReducer';
import { reducer as formReducer } from 'redux-form';
import { routerReducer } from 'react-router-redux';

const rootReducer = combineReducers({
	form: formReducer,
	routing: routerReducer,
    user: userReducer,
    loader:loaderReducer
});

export default rootReducer;