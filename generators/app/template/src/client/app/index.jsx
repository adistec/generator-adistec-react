import React from 'react';
import ReactDOM from 'react-dom';
import {Router, browserHistory } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux'
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Provider } from 'react-redux';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import routes from './constants/routes.jsx';
import DevTools from './config/devtools';
import {store} from './config/store.js';
import { getTranslations } from './config/translations.js'
import './assets/main.scss';
import {IntlProvider} from 'adistec-react-components';
import {addLocaleData} from 'react-intl';
import es from 'react-intl/locale-data/es';
import pt from 'react-intl/locale-data/pt';


injectTapEventPlugin({
    shouldRejectClick: () => document.body.querySelector('.Select-menu-outer')
});

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: '#005fab',
        primary2Color: '#008fcb',
        accent1Color: '#005fab'
    },
    datePicker: {
        selectColor: '#005fab',
        color: '#005fab'
    },
    flatButton: {
        primaryTextColor: '#005fab'
    }
});

const translations = getTranslations();
const devTools = _ENV === 'dev' || 'sit' ? <DevTools /> : null;
addLocaleData([...es, ...pt]);
const history = syncHistoryWithStore(browserHistory, store);

ReactDOM.render(
  <Provider store={store}>
    <IntlProvider locale={"en"} messages={translations}>
      <MuiThemeProvider muiTheme={muiTheme}>
            <div>
                {devTools}
                <Router history={history} routes={routes} />
            </div>
      </MuiThemeProvider>
    </IntlProvider>
  </Provider>
, document.getElementById('app'));