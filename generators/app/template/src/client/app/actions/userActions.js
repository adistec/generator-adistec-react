import { axios } from './axiosWrapper';
import { USER_INFO } from '../actions/types';

//Delete Me
const user = {
	name: 'Change Me', lastName: 'Change Me', email: 'ChangeMe@email.com', open: false, locale: 'en', auth: true, roles:[],permissions:['ADISTEC_REACT_COMPONENTS_USER']
}

export const getUserInfo = () => {
    return dispatch => {
        dispatch({type: USER_INFO, payload: user}) //Delete Me
        /*axios.get('/user/me').then(response => { //Uncomment Me
            dispatch({type: USER_INFO, payload: response.data})
        })*/
    }
};