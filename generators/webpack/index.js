'use strict';

const Generators = require('yeoman-generator');


class WebpackGenerator extends Generators {
  constructor(args, options) {
    super(args, options);
    this.environments = [
      {
        name:'dev',
        devServer:true,
        source:true,
      },
      {
        name:'sit',
        source:true
      },
      {
        name:'prod'
      }
    ];
    debugger;
  }

  writing() {
  
    // Write webpack/webpack-common.config.js
    this.fs.copyTpl(
      this.templatePath('./webpack-common.config.js'),
      this.destinationPath('webpack/webpack-common.config.js'),
      { companyName: this.options.companyName,appName: this.options.appName }
    );

    // Write webpack/webpack-dev|sit|prod.config.js
    this.environments.map(environment => 
      this.fs.copyTpl(
        this.templatePath('./webpack-x.config.js'),
        this.destinationPath(`webpack/webpack-${environment.name}.config.js`),
        { environment: environment ,appName: this.options.appName}
      )
    )
  }
}

module.exports = WebpackGenerator;
