let path = require('path');
let webpack = require('webpack');
let BUILD_DIR = path.resolve(__dirname, '../src/server/static/js');
let APP_DIR = path.resolve(__dirname, '../src/client/app');
let SERVER_DIR = path.resolve(__dirname, '../src/server/static');

const webpackMerge = require('webpack-merge');
const commonConfig = require('./webpack-common.config.js');


module.exports = webpackMerge(commonConfig, {
    output: {
        path: SERVER_DIR,
        filename: '[name][hash].js',
        chunkFilename: '[name][hash].js',
        publicPath: '/'
    },
    plugins: [
        new webpack.DefinePlugin({
            _API: JSON.stringify('/<%= appName %>-api'),
            _ENV: JSON.stringify('<%= environment.name %>')
        }),
        new webpack.HotModuleReplacementPlugin()
    ],<% if (environment.devServer) {%> 
    devServer: {
        historyApiFallback: true,
        port: 3000,
        contentBase: SERVER_DIR,
        hot: true,
        proxy: [
            {
            context:['/<%= appName %>-api/**'],
            target: 'http://127.0.0.1:8080',
            secure: false,
            changeOrigin: true
            }
        ]
    },
    watch: true<% } %><% if (environment.source) {%>,
    devtool:"source-map"
    <% } %>
});